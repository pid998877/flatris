FROM node

RUN mkdir /app
WORKDIR /app
COPY package.json /app
RUN yarn install

COPY . /app

WORKDIR /app
#RUN yarn test - it's in gitlab stage
RUN yarn build

CMD yarn start
EXPOSE 3000
